#!/usr/bin/zsh

    ####################
    # oq_docker
    ####################
    #  ZSH plugin that display status of project containers
    # https://github.com/sroze/docker-compose-zsh-plugin

    # https://github.com/robbyrussell/oh-my-zsh/tree/master/plugins/docker-compose
    # https://github.com/robbyrussell/oh-my-zsh/tree/master/plugins/docker

    #  docker-enter command shell completion
    # https://github.com/primait/docker-enter-completion

    #  oh-my-zsh plugin with docker convenience functions
    # https://github.com/johnlabarge/docker_fun

    #  Miscellaneous utility scripts and aliases for use with Docker.
    # https://github.com/unixorn/docker-helpers.zshplugin

    #  zsh completion for docker
    # https://github.com/felixr/docker-zsh-completion
    #
    #This will add docker completion for docker and currently support displaying
    #the list of containers and images. If you make any changes, please feel free
    #to pull request !
    #https://github.com/srijanshetty/docker-zsh
